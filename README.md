# PIII e PCIII #

## Notas Infos Discussões ##

Vou tentar publicar as notas dos trabalhos amanha a noite



## Notas ##


Caros alunos devido a um bug do xCode quando corre em VMs, devem ignorar os files *.SKS


## Trabalhos ##

[Trabalho 1](https://bitbucket.org/GoncaloF/istecp316/raw/53b87fc27782550bcaf812878d850edfa20f2736/fichas%20de%20trabalho/TP1.pdf)

[Trabalho 2](https://bitbucket.org/GoncaloF/istecp316/raw/e2c75ab9ff9d0ba28f4a04db35ad12bd12c019bb/fichas%20de%20trabalho/TP2.pdf)


## Aulas ##

[Intrudução](https://bitbucket.org/GoncaloF/istecp316/raw/3459022dc6a95f788d8893d1c6469399c1c66986/slides/intruducao.pdf)

[Revisões](https://bitbucket.org/GoncaloF/istecp316/raw/74afa7f612e500a99491606244cb0a66a560563a/slides/Aula1.pdf)

[Playground](https://bitbucket.org/GoncaloF/istecp316/raw/3459022dc6a95f788d8893d1c6469399c1c66986/slides/aula2.pdf)

[Swift intrudução](https://bitbucket.org/GoncaloF/istecp316/raw/3459022dc6a95f788d8893d1c6469399c1c66986/slides/aula3.pdf)

[Swift class](https://bitbucket.org/GoncaloF/istecp316/raw/68aa4b63019ac1c18cf79ba667f8f7f506a28380/slides/aula4.pdf)

[Anatomy of an app](https://bitbucket.org/GoncaloF/istecp316/raw/9d20841b286dd39b119e5111338591b59f1814b6/slides/anatomy_of_an_app.pdf)


[iOS Technologies](https://bitbucket.org/GoncaloF/istecp316/raw/9d20841b286dd39b119e5111338591b59f1814b6/slides/iOSTechnologies%20.pdf)

[SpriteKit](https://bitbucket.org/GoncaloF/istecp316/raw/9d20841b286dd39b119e5111338591b59f1814b6/slides/spirteKit.pdf)


## Exemplos ##

### Palygound ###


### Interface grafica ###

[olaMundo](https://bitbucket.org/GoncaloF/istecp316/raw/3da229e6786acdccafde3f03f2a8350c4af09c99/codigo/olaMundo.zip)

[guessTheNumber](https://bitbucket.org/GoncaloF/istecp316/raw/f4f1c15251525cf200f7bbdf5c0e7079f744b9b1/codigo/gessTheNumber.zip) 

ColorSelect  [versão 1](https://bitbucket.org/GoncaloF/istecp316/raw/a20a2ba145445f649ad002cb03e199cc58c1d916/codigo/colorSelector.zip) [versão 2](https://bitbucket.org/GoncaloF/istecp316/raw/a20a2ba145445f649ad002cb03e199cc58c1d916/codigo/colorSelectorv2.zip)

[Dismiss Keyboard](https://bitbucket.org/GoncaloF/istecp316/raw/a20a2ba145445f649ad002cb03e199cc58c1d916/codigo/keyDemo.zip)

[Menus](https://bitbucket.org/GoncaloF/istecp316/raw/a20a2ba145445f649ad002cb03e199cc58c1d916/codigo/menus.zip)

[Table View](https://bitbucket.org/GoncaloF/istecp316/raw/a20a2ba145445f649ad002cb03e199cc58c1d916/codigo/tabelv.zip)

[Imagens](https://bitbucket.org/GoncaloF/istecp316/raw/a20a2ba145445f649ad002cb03e199cc58c1d916/codigo/imgs.zip)

[Persistência](https://bitbucket.org/GoncaloF/istecp316/raw/a20a2ba145445f649ad002cb03e199cc58c1d916/codigo/persistencia.zip)

[Storyboard](https://bitbucket.org/GoncaloF/istecp316/raw/a20a2ba145445f649ad002cb03e199cc58c1d916/codigo/sotryb.zip)

Game Dev [V1](https://bitbucket.org/GoncaloF/istecp316/raw/385d1181629bd9753d3c1fd6faf9ef8e94de302f/codigo/gameDev1.zip) [V2](https://bitbucket.org/GoncaloF/istecp316/raw/acbddec341e90f1dfeb03a8bdf2fb9a687709acd/codigo/gameV2_1.zip)

## Fichas de trabalho ##

[Revisões Multimedia dia](https://bitbucket.org/GoncaloF/istecp316/raw/e6ef36da9d64df51f6b8046978c82a5c41306c15/fichas%20de%20trabalho/multimediaEx.pdf)

[Ficha 1](https://bitbucket.org/GoncaloF/istecp316/raw/3459022dc6a95f788d8893d1c6469399c1c66986/fichas%20de%20trabalho/ficha1.pdf)

[Ficha 2](https://bitbucket.org/GoncaloF/istecp316/raw/b2e801b74743d1d1a40a5d19f4dbcc40b67330fe/fichas%20de%20trabalho/ficha2.pdf)

[Ficha 3](https://bitbucket.org/GoncaloF/istecp316/raw/50e557dd9923e2547a7217fded7097db9b5b419b/fichas%20de%20trabalho/ficha3.pdf)

[Teste Exemplo](https://bitbucket.org/GoncaloF/istecp316/downloads/exame_ISTEC_exemplo.pdf)

### Soluções das fichas ###

[Ficha 1](https://bitbucket.org/GoncaloF/istecp316/raw/bc48bc313fa3225dfa9ca2d5218faf51d1a65785/codigo/ficha1.playground.zip)

[Ficha 2](https://bitbucket.org/GoncaloF/istecp316/raw/bc48bc313fa3225dfa9ca2d5218faf51d1a65785/codigo/ficha2_1.playground.zip)